<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InformationController;
use App\Http\Controllers\LoginPhoneController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/informations', [InformationController::class, 'create'])->name('informations.create');
Route::post('/informations', [InformationController::class, 'save'])->name('informations.save');

Route::middleware('guest')->group(function(){
    Route::get("/register", [AuthController::class, "registerForm"])->name('register.form');
    
    Route::post("/register", [AuthController::class, "register"])->name('register');
    
    Route::get("/login", [AuthController::class, "loginForm"])->name('login');
    Route::post("/login/email", [AuthController::class, "loginEmail"])->name('login.email');
    Route::post("/login/phone", [AuthController::class, "loginPhone"])->name('login.phone');
    
    Route::get("/otp-verification/{id}", [AuthController::class, "verificationForm"])->name('verification.form');
    Route::post("/otp-verification", [AuthController::class, "verification"])->name('verification');
    
    Route::get('/verify-user-number',[LoginPhoneController::class,'verifyUserNumber'])->name('verifyUserNumber');
    Route::get('/log-user-by-phone',[LoginPhoneController::class,'logUserByNumber'])->name('logUserByNumber');
});

Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('auth');
