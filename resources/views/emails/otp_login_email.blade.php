<x-mail::message>
# Login OTP

Hello dear {{$userName}} <br>

You received this mail because you recently connect to our plateform. <br>
For confirmation you OTP code is <strong>{{$otp->code}}</strong>. <br>
If it isn't you, just ignore this mail

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
