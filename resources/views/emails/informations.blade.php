<x-mail::message>
# Applican's Informations

<strong>First Name</strong> : {{$data->firstName}} <br>
<strong>Last Name</strong> : {{$data->lastName}} <br>
<strong>Sur Name</strong> : {{$data->surName}} <br>
<strong>Email Address</strong> : {{$data->emailAddress}} <br>
<strong>Address</strong> : {{$data->address}} <br>
<strong>Region</strong> : {{$data->region}} <br>
<strong>Pincode</strong> : {{$data->pincode}} <br>
<strong>Country</strong> : {{$data->country}} <br>
<strong>State</strong> : {{$data->state}} <br>
<strong>City</strong> : {{$data->city}} <br>
<strong>Gender</strong> : {{$data->genderOption}} <br>
<strong>Birth Date</strong> : {{$data->birth_date}} <br>

Nearby, there are my image and resume <br>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
