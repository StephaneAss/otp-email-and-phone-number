@extends('layout')

@section('title', 'Login Form')

@section('content')
    <section class="vh-100">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card shadow-2-strong" style="border-radius: 1rem;">
                        <div class="card-body p-5">
            
                            <h3 class="mb-5 text-center">Login in</h3>
                            <!-- Pills navs -->
                            <ul class="nav nav-pills nav-justified mb-3" id="ex1" role="tablist">
                                <li class="nav-item" role="presentation">
                                <a class="nav-link @if(blank($errors->all()) || $errors->has('email')) active @endif" id="tab-login" data-mdb-toggle="pill" href="#pills-login" role="tab"
                                    aria-controls="pills-login" aria-selected="true">Email</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                <a class="nav-link @if($errors->has('phoneNumber')) active @endif" id="tab-register" data-mdb-toggle="pill" href="#pills-register" role="tab"
                                    aria-controls="pills-register" aria-selected="false">Phone Number</a>
                                </li>
                            </ul>
                            <!-- Pills navs -->
  
                            <!-- Pills content -->
                            <div class="tab-content">
                                <div class="tab-pane fade @if(blank($errors->all()) || $errors->has('email')) show active @endif" id="pills-login" role="tabpanel" aria-labelledby="tab-login">
                                    <form method="POST" action="{{route('login.email')}}">
                                        @csrf
                                        <!-- Email input -->
                                        <div class="mb-4">
                                            <div class="form-outline">
                                                <input type="email" id="email" name="email" class="form-control" />
                                                <label class="form-label" for="email">Email</label>
                                            </div>
                                            <x-error field="email" />
                                        </div>
                                        <!-- Submit button -->
                                        <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>
                                        <!-- Register buttons -->
                                        <div class="text-center">
                                            <p>Not register yet ? <a href="{{route('register.form')}}">Register</a></p>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade @if($errors->has('phoneNumber')) show active @endif" id="pills-register" role="tabpanel" aria-labelledby="tab-register">
                                    <form id="phone-form">
                                        @csrf
                                        <!-- Name input -->
                                        <div class="mb-4">
                                            <div class="form-outline">
                                                <input type="tel" id="phoneNumber" name="phoneNumber" class="form-control" />
                                                <label class="form-label" for="phoneNumber">Phone Number</label>
                                            </div>
                                            <span id="phone-error" class="text-danger"></span>
                                        </div>
                                        <!-- Recaptcha -->
                                        <div class="mb-2" id="recaptcha-container"></div>
                                
                                        <!-- Submit button -->
                                        <button type="button" id="button-phone" onclick="sendOTP()" class="btn btn-primary btn-block mb-3">Sign in</button>
                                        <!-- Register buttons -->
                                        <div class="text-center">
                                            <p>Not register yet ? <a href="{{route('register.form')}}">Register</a></p>
                                        </div>
                                    </form>
                                    <form id="confirm-phone-otp" style="display: none">
                                        @csrf
                                        <!-- Name input -->
                                        <div class="mb-4">
                                            <div class="form-outline">
                                                <input type="text" id="code" name="code" class="form-control" />
                                                <label class="form-label" for="code">OTP</label>
                                            </div>
                                            <span id="code-error" class="text-danger"></span>
                                        </div>
                                
                                        <!-- Submit button -->
                                        <button type="button" id="verify_code" onclick="verifyOTP()" class="btn btn-primary btn-block mb-3">Verify</button>
                                        
                                    </form>
                                </div>
                            </div>
                            <!-- Pills content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="{{asset('assets/js/firebase.js')}}"></script>
    <script src="{{asset('assets/js/firebase_setting.js')}}"></script>
    <script src="{{asset('assets/js/interate_with_firebase.js')}}"></script>
@endpush