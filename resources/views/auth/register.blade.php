@extends('layout')

@section('title', 'Register Form')

@section('content')
    <section class="vh-100">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card shadow-2-strong" style="border-radius: 1rem;">
                        <div class="card-body p-5">
            
                            <h3 class="mb-5 text-center">Sign in</h3>
                            <form action="{{route('register')}}" method="POST">
                                @csrf
                                <div class="mb-4">
                                    <div class="form-outline ">
                                        <input type="text" id="userName" name="userName" value="{{old('userName')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="userName">User Name</label>
                                    </div>
                                    <x-error field="userName" />
                                </div>
                                <div class="mb-4">
                                    <div class="form-outline">
                                        <input type="email" id="email" name="email" value="{{old('email')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="email">Email</label>
                                    </div>
                                    <x-error field="email" />
                                </div>
                                <div class="mb-4">
                                    <div class="form-outline">
                                        <input type="tel" id="phoneNumber" name="phoneNumber" value="{{old('phoneNumber')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="phoneNumber">Phone Number</label>
                                    </div>
                                    <x-error field="phoneNumber" />
                                </div>

                                <button class="btn btn-primary btn-lg btn-block mb-4" type="submit">Sign In</button>
                                <!-- Login buttons -->
                                <div class="text-center">
                                    <p>Already registered ? <a href="{{route('login')}}">Login In</a></p>
                                </div>
                            </form>
                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection