@extends('layout')

@section('title', 'Register Form')

@section('content')
    <section class="vh-100">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card shadow-2-strong" style="border-radius: 1rem;">
                        <div class="card-body p-5">
            
                            <h3 class="mb-5 text-center">OTP Verification</h3>
                            <form action="{{route('verification')}}" method="POST">
                                @csrf
                                <div class="mb-4">
                                    <div class="form-outline ">
                                        <input type="number" id="code" name="code" value="{{old('code')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="code">OTP</label>
                                    </div>
                                    <x-error field="code" />
                                </div>
                                <input type="hidden" name="user_id" value="{{$user_id}}">

                                <button class="btn btn-primary btn-lg btn-block" type="submit">Verify</button>
                            </form>
                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection