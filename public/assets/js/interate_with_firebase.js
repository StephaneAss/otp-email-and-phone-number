window.onload = function () {
    render();
};

function render() {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    recaptchaVerifier.render();
}

function sendOTP() {
    let phone = $('#phoneNumber').val();

    console.log(phone);

    ajaxHeader();

    $.get("verify-user-number", {phone : phone}, function(){
        $("#button-phone").text("Loading...");
    }).done(function(data){
        console.log(data)
        
        if (data.success==true) {
            //phone = "+"+phone.replace("+", "")
            //console.log(phone)
            $("#button-phone").text("SIGN IN");
            firebase.auth().signInWithPhoneNumber(phone, window.recaptchaVerifier).then(function (confirmationResult) {
                window.confirmationResult = confirmationResult;
                coderesult = confirmationResult;
                $("#phone-form").hide();
                $("#confirm-phone-otp").show();
            }).catch(function (error) {
                console.log(error)
                $("#button-phone").text("SIGN IN");
                $("#confirm-phone-otp").hide();
                $("#phone-form").show();
                console.log(error.message)
                $("#phone-error").text(error.message)
            });
        } else {
            $("#button-phone").text("SIGN IN");
            $("#phone-error").text(data.message)
        }
    }).fail(function(error) {
        $("#button-phone").text("SIGN IN");
        $("#phone-error").text(error.message)
    })
}

function verifyOTP() {
    $("#verify_code").text("Loading...");
    var phone = $("#phoneNumber").val();
    
    var code = $("#code").val();
    coderesult.confirm(code).then(function (result) {
        var user = result.user;
        ajaxHeader()
        //phone = phone.replace("+", "")
        console.log('contact', phone)
        $.get("log-user-by-phone", {phone : phone}, function(){
            console.log("success")
        })
        .done(function(data){
            $("#verify_code").text("Verify")
            console.log(data)
            if (data.success) {
                window.location.replace('home')
            }
        })
        .fail(function(error) {
            $("#code-error").text(error.message)
            $("#verify_code").text("Verify")
        })
    }).catch(function (error) {
        $("#code-error").text(error.message);
        $("#verify_code").text("Verify");
    });
}

function ajaxHeader() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}