# Projet to send Applican informations

## Commands to execute
- **php artisan db:wipe** to clear the database
- **php artisan migrate** to fill the database
- **php artisan db:seed** to fill countries table

## configuration
- You must configure your smtp information in the .env to allow sending mail

## The url to see the information form
- **/informations**

## The url to register
- **/register**
## The url to login
- **/login**

## Autor
### Name
**Stephane ASSOCLE**

### Contacts
- **+229 67710659**
- **stephaneassocle@gmail.com**
