<?php

namespace App\Providers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class ServerStopProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        if (PHP_OS_FAMILY === 'Windows') {
            declare(ticks=1);

            pcntl_signal(SIGINT, function () {
                // Custom shutdown logic here
                $this->actionWhenServerStop();
                exit();
            });

            while (true) {
                // Handle signals
                pcntl_signal_dispatch();

                // Sleep for a short time to avoid high CPU usage
                usleep(1000);
            }
        } else {
            pcntl_async_signals(true);

            pcntl_signal(SIGINT, function () {
                // Custom shutdown logic here
                $this->actionWhenServerStop();
                exit();
            });
        }
    }

    public function actionWhenServerStop()
    {
        echo "Server stopped";

        Artisan::call("key:generate");
    }
}
