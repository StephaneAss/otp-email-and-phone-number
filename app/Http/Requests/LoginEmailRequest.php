<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class LoginEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email'
        ];
    }

    public function after(): array
    {
        $user = User::whereEmail($this->email)->first();
        return [
            function (Validator $validator) use($user) {
                if (blank($user)) {
                    $validator->errors()->add(
                        'email',
                        'User not found'
                    );
                }
            }
        ];
    }
}
