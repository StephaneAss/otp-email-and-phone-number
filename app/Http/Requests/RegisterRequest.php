<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'userName' => 'required|string|min:3|max:20|unique:users',
            'phoneNumber' => 'required|string|min:8|max:20|unique:users',
            'email' => 'required|email|unique:users',
        ];
    }

    /**
     * Get the "after" validation callables for the request.
     */
    public function after(): array
    {
        $phone = substr($this->phoneNumber, 0, 1);
        $space = explode(' ', $this->phoneNumber);
        //dd($phone);
        return [
            function (Validator $validator) use($phone, $space){
                if ($phone != '+' || count($space) > 1) {
                    $validator->errors()->add(
                        'phoneNumber',
                        'Your phone number must start with + without space'
                    );
                }
            }
        ];
    }
}
