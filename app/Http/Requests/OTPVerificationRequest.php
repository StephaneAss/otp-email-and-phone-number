<?php

namespace App\Http\Requests;

use App\Models\Otp;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class OTPVerificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'code' => "required"
        ];
    }

    public function after(): array
    {
        $otp = Otp::where('user_id', $this->user_id)->where('code', $this->code)->latest()->first();

        return [
            function (Validator $validator) use($otp) {
                if (blank($otp)) {
                    $validator->errors()->add(
                        'code',
                        'Your OTP is not correct.'
                    );
                }
            }
        ];
    }
}
