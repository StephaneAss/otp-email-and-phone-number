<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginPhoneController extends Controller
{
    public function verifyUserNumber(Request $request)
    {
        $user = User::where('phoneNumber', $request->phone)->first();
        //dd($user, );
        if (!blank($user)) {
            return response()->json([
                "success"=>true,
                "message"=>"User found"
            ], 200);
        }

        return response()->json([
            "success"=>false,
            "message"=>"Phone number not found (start with + when registering without space)"
        ], 200);
    }

    public function logUserByNumber(Request $request)
    {
        $user = User::where('phoneNumber', $request->phone)->first();
        //return response()->json($user);
        //dd($request->phone, $user);
        Auth::login($user);
        if (Auth::check()) {
            return response()->json([
                "success"=>true,
                "message"=>"User connected"
            ], 200);
        }

        return response()->json([
            "success"=>false,
            "message"=>"User not connected"
        ], 200);
    }
}
