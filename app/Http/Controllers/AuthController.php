<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginEmailRequest;
use App\Http\Requests\LoginPhoneRequest;
use App\Http\Requests\OTPVerificationRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Otp;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function registerForm()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {//dd($request->except('_token'));
        User::create($request->except('token'));

        return response()->redirectToRoute('login');
    }

    public function loginForm()
    {
        return view('auth.login');
    }

    public function loginEmail(LoginEmailRequest $request)
    {
        $user = User::whereEmail($request->email)->first();

        $user->generateOTP();

        return redirect()->route('verification.form', ['id'=> $user->id]);
    }
    
    public function verificationForm($id)
    {
        return view('auth.verification', ['user_id' => $id]);
    }

    public function verification(OTPVerificationRequest $request)
    {
        $otp = Otp::where('user_id', $request->user_id)->where('code', $request->code)->latest()->first();

        $now = now();

        if (!blank($otp) && $now->isAfter($otp->expire_at)) {
            return redirect()->route('login')->with('error', 'Your OTP has been expired');
        }

        $user = User::find($request->user_id);

        if(!blank($user)){
            $otp->update([ 'expire_at' => now()  ]);

            Auth::login($user);

            return redirect('/home');
        }

        return redirect()->route('login')->with('error', 'Your Otp is not correct');
    }
}
