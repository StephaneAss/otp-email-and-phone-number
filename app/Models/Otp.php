<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function getExistingOTP($user_id) : null|self
    {
        $otp = Otp::where('user_id', $user_id)->latest()->first();

        $now = now();

        if($otp && $now->isBefore($otp->expire_at)){
            return $otp;
        }
        return null;
    }

    public static function generate($user_id)
    {
        $now = now();

        return Otp::create([
            'user_id' => $user_id,
            'code' => rand(123456, 999999),
            'expire_at' => $now->addMinutes(10)
        ]);
    }
}
